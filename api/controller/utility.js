var express = require("express");
var router = express.Router();
var db = require('../db');

/*
JSON EXAMPLE
{
  name: 'Smoke Long',
  description: 'Smoke the long so we can go out of long doors',
  site: 't',
  spam_type: 'smoke',
  gif_url: 'https://media.giphy.com/media/KpAPQVW9lWnWU/giphy.gif',
  start_Location: [ { x: 696 }, { y: 798 } ],
  end_Location: [ { x: 424 }, { y: 236 } ]
}



 */

/**
 * Create Utility
 */
router.post("/", function(req, res) {
    //console.log(req.body);
    var json = JSON.stringify(req.body);
    console.log("TESSSSTEETEET");
    json = JSON.parse(json);
    var name = json.name;
    var desc = json.description;
    var site = json.site;
    var spam_type = json.spam_type;
    var gif_url = db.escape(json.gif_url);
    var start_Location = json.start_Location;
    var end_Location = json.end_Location;

    console.log(`Teste:  ${JSON.stringify(start_Location)}`);
    //start_Location = start_Location.x;
    console.log(`TesteRERERERRE: ${start_Location.x}`);
    var sql = `INSERT INTO utility (id, name, description, type, map_id, map_start_x, map_start_y, map_end_x, map_end_y, location, url) VALUES
                                (1, "${name}", "${desc}", "${spam_type}", 1, "${start_Location.x}", "${start_Location.y}", "${end_Location.x}", "${end_Location.y}", "${site}", ${gif_url})`;

    db.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Data inserted!, ID: " + result.insertId); //Data inserted!, ID: 1
        res.send("Data inserted!, ID: " + result.insertId); //Data inserted!, ID: 1
    });

    //res.send(`I received your POST request. This is what you sent me`);
});

/*router.put("/", async (req, res) => {
    console.log(req.body);
    var json = JSON.stringify(req.body);
    res.send(`I received your POST request. This is what you sent me: ${json}`);
});*/

/**
 * Get All utility
 */
router.get("/", function(req, res) {
    console.log(req.body);
    var json = JSON.stringify(req.body);
    res.send(`I received your POST request. This is what you sent me: ${json}`);
});

/**
 * Get specific utility
 */
router.get("/{:id}", function(req, res) {
    console.log(req.body);
    var json = JSON.stringify(req.body);
    res.send(`I received your POST request. This is what you sent me: ${json}`);
});

/**
 * Get all map utility
 */
router.get("/{:map_id}", function(req, res) {
    console.log(req.body);
    var json = JSON.stringify(req.body);
    res.send(`I received your POST request. This is what you sent me: ${json}`);
});

/*
Put -> Update/Replace
Patch -> Update/Modify

Provavelmente usar o Modify. Do frontEnd para o Backend mandar o antigo e os novos campos e verificar se os campos inalterados são iguais, para evitar alguem dar inject ao request.
*/

/**
 * Update utility
 */
router.put("/{:id}", function(req, res) {
    console.log(req.body);
    var json = JSON.stringify(req.body);
    res.send(`I received your POST request. This is what you sent me: ${json}`);
});

/**
 * Delete utility
 */
router.delete("/{:id}", function(req, res) {
    console.log(req.body);
    var json = JSON.stringify(req.body);
    res.send(`I received your POST request. This is what you sent me: ${json}`);
});

module.exports = router;