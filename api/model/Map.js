function Map(id, name, type, mapImage) {
    this.name = name;
    this.type = type;
    this.mapImage = mapImage;
}

Map.prototype.getName = function () {
    return this.name;
}

Map.prototype.setName = function(nName){
    this.name = nName;
}

Map.prototype.getType = function () {
    return this.type;
}

Map.prototype.setType = function(nType){
    this.type = nType;
}

Map.prototype.getMapImage = function () {
    return this.mapImage;
}

Map.prototype.setMapImage = function(nMapImage){
    this.mapImage = nMapImage;
}