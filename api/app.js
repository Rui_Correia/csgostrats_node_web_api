var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require("cors");

var indexRouter = require('./routes/index');
var testAPIRouter = require("./routes/testeAPI");

var userRouter = require('./controller/user');
var utilityRouter = require("./controller/utility");
var mapRouter = require("./controller/map");
var teamRouter = require("./controller/team");
var stratRouter = require("./controller/strat");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use("/testeAPI", testAPIRouter);

app.use('/api/user', userRouter);
app.use("/api/utility", utilityRouter);
app.use("/api/map", mapRouter);
app.use("/api/team", teamRouter);
app.use("/api/strat", stratRouter);
// catch 404 and forward to error handler

app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
