import React from 'react';
import './App.css';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ReactPlayer from 'react-player'

import smoke from "./smoke.png";
import flash from "./flashbang.png";
import nade from "./nade.png";
import molotov from "./molly.png";
import decoy from "./decoy.png";
import start from "./start.png";

/*
Function ex:

function App (){

const [state, setState] = React.useState({
  name: '',
  desc: '',
  spam_type:'',
  gif_url:''
});*/

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '',
      desc: '',
      site:'',
      spam_type:'',
      gif_url:'',
      start_loc_bool: false,
      end_loc_bool: false,
      spam_item_size: 25,
      start_x: undefined,
      start_y: undefined,
      end_x: undefined,
      end_y: undefined,
      apiResponse: '',


      url: null,
      pip: false,
      playing: true,
      controls: false,
      light: false,
      volume: 0.8,
      muted: false,
      played: 0,
      loaded: 0,
      duration: 0,
      playbackRate: 1.0,
      loop: false};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeUrl = this.handleChangeUrl.bind(this);
    this.handleCoords = this.handleCoords.bind(this);
    this.handleStartLoc = this.handleStartLoc.bind(this);
    this.handleEndLoc = this.handleEndLoc.bind(this);
  }

  callAPI() {
    fetch("http://localhost:9000/testeAPI")
        .then(res => res.text())
        .then(res => this.setState({ apiResponse: res }));
  }

  componentWillMount() {
    this.callAPI();
  }

  handleSubmit = async event => {
    event.preventDefault();

    debugger;
    if (this.state.start_x == null) {
      alert("Preencha a start loc!")
    } else if(this.state.end_x == null) {
      alert("Preencha a end loc!")
    } else {
        let json = {
          name: this.state.name,
          description: this.state.desc,
          site: this.state.site,
          spam_type: this.state.spam_type,
          gif_url: this.state.gif_url,
          start_Location: {x: this.state.start_x * 2 , y: this.state.start_y * 2},
          end_Location: {x: this.state.end_x , y: this.state.end_y}
        };

        alert(JSON.stringify(json));

        /*fetch('http://localhost:9000/api/utility' ,{
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(json),
        })
            .then(res => res.text())
            .then(res =>  this.setState({ apiResponse: res }));
*/
      }
  }
  
  handleChange(event) {
    const value = event.target.value;
    this.setState({
      ...this.state, [event.target.name]:value
    })

    if(event.target.name === "spam_type"){
      if(value !== ''){
        document.getElementById("start_lbl").style.display = 'inline'
        document.getElementById("end_lbl").style.display = 'inline'
      }else{
        document.getElementById("start_lbl").style.display = 'none'
        document.getElementById("end_lbl").style.display = 'none'
      }
    }
  }

  handleChangeUrl(event) {
    const value = event.target.value;
    this.setState({
      ...this.state, [event.target.name]:value
    });
    let realGif = document.getElementsByName('real_gif');
    /*let img = new Image();
    img.src = value;
    img.onerror = function() {realGif[0].style.display = 'none';};
    img.onabort = function() {realGif[0].style.display = 'none';};
    img.onload = function() {*/
      //realGif[0].style.display = 'inline';
    //debugger;
    //  realGif[0].src = value;
    //}
  }

  handleStartLoc(e) {
    let color = undefined;

    if(this.state.end_loc_bool){
      this.setState({end_loc_bool:false});
      let endLoc = document.getElementsByName('end_btn');
      endLoc[0].style.backgroundColor = 'red';
    }

    if(this.state.start_loc_bool===false){
      this.setState({start_loc_bool:true});
      color = 'green';
    }else{
      this.setState({start_loc_bool:false});
      color = 'red';
    }

    let startLoc = document.getElementsByName('start_btn');
    startLoc[0].style.backgroundColor = color;

    e.preventDefault();
  }

  handleEndLoc(e) {
    e.preventDefault();
    let color = undefined;

    if(this.state.start_loc_bool){
      this.setState({start_loc_bool:false});
      let endLoc = document.getElementsByName('start_btn');
      endLoc[0].style.backgroundColor = 'red';
    }

    if(this.state.end_loc_bool===false){
      this.setState({end_loc_bool:true});
      color = 'green';
    }else{
      this.setState({end_loc_bool:false});
      color = 'red';
    }

    let endLoc = document.getElementsByName('end_btn');
    endLoc[0].style.backgroundColor = color;
  }

  handleCoords(e) {

    let x = e.nativeEvent.offsetX;
    let y = e.nativeEvent.offsetY;

    if(this.state.start_loc_bool) {

      this.setState({start_x:x});
      this.setState({start_y:y});

      let center_x=x-this.state.spam_item_size/2;
      let center_y=y-this.state.spam_item_size/2;

      //TODO icon de inicio
      let spamType = <img src={start} alt="spam" style={{position:'absolute', top: center_y, left: center_x, height: this.state.spam_item_size+'px' }}/>
      ReactDOM.render(spamType, document.getElementById("mapDiv"))

      document.getElementsByName('start_locs')[0].innerText = "Starting X= " + x + " / Y= " + y;
      let endLoc = document.getElementsByName('start_btn');
      endLoc[0].style.backgroundColor = 'red';
      this.setState({start_loc_bool:false});
    }

    if(this.state.end_loc_bool) {

      this.setState({end_x:x});
      this.setState({end_y:y});

      let center_x=x-this.state.spam_item_size/2;
      let center_y=y-this.state.spam_item_size/2;

      //Switch-case tipo de spam
      let spamIcon = ''

      if(this.state.spam_type === "smoke"){
        spamIcon = smoke
      }else if(this.state.spam_type === "flash"){
        spamIcon = flash
      }else if(this.state.spam_type === "molotov"){
        spamIcon = molotov
      }else if(this.state.spam_type === "nade"){
        spamIcon = nade
      }else if(this.state.spam_type === "decoy"){
        spamIcon = decoy
      }

      console.log(spamIcon)

      //let spamType = <Spam_Icon src = {spamIcon} x={center_x} y={center_y} spam_item_size={this.state.spam_item_size}/>;
      let spamType = <img src={spamIcon} alt="spam" style={{position:'absolute', top: center_y, left: center_x, height: this.state.spam_item_size+'px' }}/>

      ReactDOM.render(spamType, document.getElementById("mapDiv"))

      document.getElementsByName('end_locs')[0].innerText = "Ending X= " + x + " / Y= " + y;
      let endLoc = document.getElementsByName('end_btn');
      endLoc[0].style.backgroundColor = 'red';
      this.setState({end_loc_bool:false})
    }
  }

//https://www.npmjs.com/package/react-player
  render() {
    return (
        <div className="App">
          <header className="App-header">
            <div id="mapDiv" className="App-logo" onClick={this.handleCoords}>
            </div>

            <form onSubmit={this.handleSubmit}>
              <label>
                <p >{this.state.apiResponse}</p>
                Name:
                <input type="text" name="name" value={this.state.name} onChange={this.handleChange} required/>
              </label>
              <br/>
              <label>
                Description:
                <input type="text" name="desc" value={this.state.desc} onChange={this.handleChange} required/>
              </label>
              <br/>
              <label>
                Site:
                <select type="text" name="site" value={this.state.site} onChange={this.handleChange} required>
                  <option value=""></option>
                  <option value="t">Terrorist</option>
                  <option value="ct">Counter-Terrorist</option>
                </select>
              </label>
              <br/>
              <label>
                Spam Type:
                <select type="text" name="spam_type" value={this.state.spam_type} onChange={this.handleChange} required>
                  <option value=""></option>
                  <option value="smoke">Smoke</option>
                  <option value="flash">Flash</option>
                  <option value="molotov">Molotov</option>
                  <option value="nade">Nade</option>
                  <option value="decoy">Decoy</option>
                </select>
              </label>
              <br/>
              <label>
                Gif url:
                <input type="text" name="gif_url" value={this.state.gif_url} onChange={this.handleChange} required/>
                <ReactPlayer className="gif" url={this.state.gif_url} playing={this.state.playing} />
              </label>
              <br/>
              <label id="start_lbl" style={{display:'none'}}>
                Start Loc: <button name="start_btn" onClick={this.handleStartLoc} className="locColorstart" required>DefineLoc</button>
                <br/>
                <label name="start_locs" required></label>
              </label>
              <br/>
              <label id="end_lbl" style={{display:'none'}}>
                End Loc: <button name="end_btn" onClick={this.handleEndLoc} className="locColorstart">DefineLoc</button>
                <br/>
                <label name="end_locs"/>
              </label>
              <br/>
              <input type="submit" value="Submit"/>
            </form>
          </header>
        </div>
    );
  }
}

function Spam_Icon(props) {
  const{icon_src,x,y,spam_item_size} = props;
  console.log(props)
  return (
      <img src={icon_src.toString()} alt="spam" style={{position:'absolute', top: y, left: x, height: spam_item_size+'px' }}/>
  );
}

export default App;


/*
 <td>
                  <input ref={input => { this.urlInput = input }} type='text' placeholder='Enter URL' />
                  <button onClick={() => this.setState({ url: this.urlInput.value })}>Load</button>
                </td>



 */
