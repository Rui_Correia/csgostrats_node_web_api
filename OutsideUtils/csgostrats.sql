-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 06:29 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `csgostrats`
--

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE `maps` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `type` text DEFAULT NULL,
  `map_image` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `strats`
--

CREATE TABLE `strats` (
  `id` int(11) NOT NULL,
  `author_id` text DEFAULT NULL,
  `name` text DEFAULT NULL,
  `side` text DEFAULT NULL,
  `map_id` int(11) NOT NULL,
  `player_1_name` text DEFAULT NULL,
  `player_2_name` text DEFAULT NULL,
  `player_3_name` text DEFAULT NULL,
  `player_4_name` text DEFAULT NULL,
  `player_5_name` text DEFAULT NULL,
  `player_1` text DEFAULT NULL,
  `player_2` text DEFAULT NULL,
  `player_3` text DEFAULT NULL,
  `player_4` text DEFAULT NULL,
  `player_5` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `strat_utility`
--

CREATE TABLE `strat_utility` (
  `utility_id` int(11) NOT NULL,
  `strat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `coach` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `team_strat`
--

CREATE TABLE `team_strat` (
  `team_id` int(11) NOT NULL,
  `strat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `team_user`
--

CREATE TABLE `team_user` (
  `team_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `token` text DEFAULT NULL,
  `valid_to` text DEFAULT NULL,
  `player_status` text NOT NULL DEFAULT 'normal',
  `register_date` date NOT NULL,
  `register_type` text NOT NULL,
  `register_source` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_strat`
--

CREATE TABLE `user_strat` (
  `strat_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `utility`
--

CREATE TABLE `utility` (
  `id` int(11) NOT NULL,
  `type` text DEFAULT NULL,
  `location` text DEFAULT NULL,
  `map_id` int(11) NOT NULL,
  `map_x` text DEFAULT NULL,
  `map_y` text DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  `url` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `maps_id_uindex` (`id`);

--
-- Indexes for table `strats`
--
ALTER TABLE `strats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `strats_id_uindex` (`id`),
  ADD KEY `strats_maps_id_fk` (`map_id`);

--
-- Indexes for table `strat_utility`
--
ALTER TABLE `strat_utility`
  ADD KEY `strat_utility_strats_id_fk` (`strat_id`),
  ADD KEY `strat_utility_utility_id_fk` (`utility_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_id_uindex` (`id`);

--
-- Indexes for table `team_strat`
--
ALTER TABLE `team_strat`
  ADD KEY `team_strat_strats_id_fk` (`strat_id`),
  ADD KEY `team_strat_team_id_fk` (`team_id`);

--
-- Indexes for table `team_user`
--
ALTER TABLE `team_user`
  ADD KEY `team_user_team_id_fk` (`team_id`),
  ADD KEY `team_user_user_id_fk` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `users_email_uindex` (`email`) USING HASH,
  ADD UNIQUE KEY `users_token_uindex` (`token`) USING HASH;

--
-- Indexes for table `user_strat`
--
ALTER TABLE `user_strat`
  ADD KEY `user_strat_strats_id_fk` (`strat_id`),
  ADD KEY `user_strat_users_id_fk` (`user_id`);

--
-- Indexes for table `utility`
--
ALTER TABLE `utility`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `utility_id_uindex` (`id`),
  ADD KEY `utility_map_id_fk` (`map_id`),
  ADD KEY `utility_users_id_fk` (`author_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `strats`
--
ALTER TABLE `strats`
  ADD CONSTRAINT `strats_maps_id_fk` FOREIGN KEY (`map_id`) REFERENCES `maps` (`id`);

--
-- Constraints for table `strat_utility`
--
ALTER TABLE `strat_utility`
  ADD CONSTRAINT `strat_utility_strats_id_fk` FOREIGN KEY (`strat_id`) REFERENCES `strats` (`id`),
  ADD CONSTRAINT `strat_utility_utility_id_fk` FOREIGN KEY (`utility_id`) REFERENCES `utility` (`id`);

--
-- Constraints for table `team_strat`
--
ALTER TABLE `team_strat`
  ADD CONSTRAINT `team_strat_strats_id_fk` FOREIGN KEY (`strat_id`) REFERENCES `strats` (`id`),
  ADD CONSTRAINT `team_strat_team_id_fk` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

--
-- Constraints for table `team_user`
--
ALTER TABLE `team_user`
  ADD CONSTRAINT `team_user_team_id_fk` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  ADD CONSTRAINT `team_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_strat`
--
ALTER TABLE `user_strat`
  ADD CONSTRAINT `user_strat_strats_id_fk` FOREIGN KEY (`strat_id`) REFERENCES `strats` (`id`),
  ADD CONSTRAINT `user_strat_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `utility`
--
ALTER TABLE `utility`
  ADD CONSTRAINT `utility_map_id_fk` FOREIGN KEY (`map_id`) REFERENCES `maps` (`id`),
  ADD CONSTRAINT `utility_users_id_fk` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
